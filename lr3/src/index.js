const { app, BrowserWindow } = require('electron');
const { ipcRenderer } = require('electron');
const path = require('path');

const os  = require('os-utils');

const createWindow = () => {
const mainWindow = new BrowserWindow({
    width: 1000,
    height: 400,
    icon: __dirname + "/images.jpg",
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true
      }    
  });

  mainWindow.loadFile(path.join(__dirname, "index.html"));
  
  setInterval(() => {
  os.cpuUsage(function(v) {
  console.log("CPU Usage (%): " + v.toFixed(4) * 100);
  mainWindow.webContents.send("cpu", v.toFixed(4) * 100);

  console.log("Mem Usage (%): " + os.freememPercentage().toFixed(4) * 100);
  mainWindow.webContents.send("mem", os.freememPercentage().toFixed(4) * 100);


  console.log("Total Mem (GB): " + Math.floor(os.totalmem() / 1024));
  mainWindow.webContents.send("total-mem", Math.floor(os.totalmem() / 1024));

  console.log("Platform: " + os.platform());
  



  
  
  });
},1000)
};

if (require('electron-squirrel-startup')) {

  app.quit();
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

